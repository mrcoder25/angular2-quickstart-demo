// --========================= >> Initial | my-app >> START >> =======--
// import { Component } from '@angular/core';

// @Component({
//   selector: 'my-app',
//   template: `<h1>Hello {{name}}</h1>`,
// })
// export class AppComponent  { name = 'MrCoder!'; }
// --========================= >> Initial | my-app >> END >> =========--

// --========================= >> angular2_components | my-sign >> START >> =======--
// import { Component } from '@angular/core';

// @Component ({
//    selector: 'my-sign',
//    template: ` <div class='sign'>
//       Hello Guys, <br/><br/>Content of AppComponent loaded successfully. <br/><br/>Do you like my work? If yes, then, Please visit my website <a target='_blank' href='http://mrcoder.in/'>{{website}}</a>. 
//       <span><br/><br/><br/>Thanks and Kind Regards,<br/><br/></span>
//       <div class='sign-name'><strong>{{name}} |</strong> <span class='sign-des'>{{designation}}</span></div>
//       <div>Cell/W'app : <span class='sign-Cell'><strong>{{cellNumber}}</strong></span></div>
//       <div>Skype : <span class='sign-skype'><strong>{{skypeAddress}}</strong> ({{cellNumber}})</span></div>
//       <div>Mail : <a target='_blank' href='mailto:mik@mrcoder.in'>{{mailAddress}}</a></div>
//       <div>Website : <a target='_blank' href='http://mrcoder.in/'>{{website}}</a></div>
//    </div> `,
// })

// export class AppComponent {
//    name: string = 'Mr. Mik Patel';
//    designation: string = 'Software Developer';
//    cellNumber: number = 9974300025;
//    skypeAddress: string = 'MrCoder';
//    mailAddress: string = 'mik@mrcoder.in';
//    website: string = 'www.mrcoder.in';
// }
// --========================= >> angular2_components | my-sign >> END >> =========--

// --========================= >> Angular2_Templates | my-sign >> START >> =======--
// import { Component } from '@angular/core';

// @Component ({
//    selector: 'my-sign',
//    templateUrl: 'app/app.component.html'
// })

// export class AppComponent {
//    name: string = 'Mr. Mik Patel';
//    designation: string = 'Software Developer';
//    cellNumber: number = 9974300025;
//    skypeAddress: string = 'MrCoder';
//    mailAddress: string = 'mik@mrcoder.in';
//    website: string = 'www.mrcoder.in';
// }
// --========================= >> Angular2_Templates | my-sign >> END >> =========--

// --========================= >> Angular2_Directives | my-directive >> START >> =======--
// import { Component } from '@angular/core';

// @Component ({
//    selector: 'my-directive',
//    templateUrl: 'app/app.component.html'
// })

// export class AppComponent {
//    name: string = 'Mr. Mik Patel';
//    designation: string = 'Software Developer';
//    cellNumber: number = 9974300025;
//    skypeAddress: string = 'MrCoder';
//    mailAddress: string = 'mik@mrcoder.in';
//    website: string = 'www.mrcoder.in';
//    isVisible: boolean = true;
//    dataList: any[] = [ {
//                         "ID": "1",
//                         "Name" : "MrCoder"
//                     },

//                     {
//                         "ID": "2",
//                         "Name" : "Mik"
//                     } ];
// }
// --========================= >> Angular2_Directives | my-directive >> END >> =========--

// --========================= >> Angular2_MrCoder | im-mrcoder >> START >> =======--
// import { Component } from '@angular/core';

// @Component ({
//    selector: 'im-mrcoder',
//    templateUrl: 'app/app.component.html'
// })

// export class AppComponent {
//    name: string = 'Mr. Mik Patel';
//    designation: string = 'Software Developer';
//    cellNumber: number = 9974300025;
//    skypeAddress: string = 'MrCoder';
//    mailAddress: string = 'mik@mrcoder.in';
//    website: string = 'www.mrcoder.in';
// }
// --========================= >> Angular2_MrCoder | im-mrcoder >> END >> =========--

// --========================= >> Angular2_Class_Extends | im-mrcoder >> START >> =======--
// import { Component } from '@angular/core';

// @Component ({
//    selector: 'im-mrcoder',
//    templateUrl: 'app/app.component.html'
// })

// export class AppComponent {
//    name: string = 'Mr. Mik Patel';
//    designation: string = 'Software Developer';
//    cellNumber: number = 9974300025;
//    skypeAddress: string = 'MrCoder';
//    mailAddress: string = 'mik@mrcoder.in';
//    website: string = 'www.mrcoder.in';
//    isVisible: boolean = true;
//    imageList: any[] = [ {
//                             "ID": "1",
//                             "Url" : "app/app_data/images/MrCoder_Icon.png"
//                         }];
// }
// --========================= >> Angular2_Class_Extends | im-mrcoder >> END >> =========--

// --========================= >> Angular2_CRUD_Operations_Using_HTTP | im-mrcoder >> START >> =======--
// import { Component, OnInit } from '@angular/core';
// import { Http, Response } from '@angular/http';
// import { Router} from '@angular/router';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';

// import { IProduct } from './content/products/products';
// import { ProductService } from './content/products/products.service';

// @Component({
//     selector: 'im-mrcoder',
//     templateUrl: 'app/app.component.html',
//     providers: [ProductService]
// })

// export class AppComponent implements OnInit{
//     name: string = 'Mr. Mik Patel';
//     designation: string = 'Software Developer';
//     cellNumber: number = 9974300025;
//     skypeAddress: string = 'MrCoder';
//     mailAddress: string = 'mik@mrcoder.in';
//     website: string = 'www.mrcoder.in';
//     isVisible: boolean = true;

//     errorMessage: string = '';
//     public iProducts: IProduct[] = [];
//     constructor(public _product: ProductService) {
//     }

//     ngOnInit(): void {
//         this._product.getProducts()
//             .subscribe(data => this.iProducts = data, error => this.errorMessage = error);
//     }
// }
// --========================= >> Angular2_CRUD_Operations_Using_HTTP | im-mrcoder >> END >> =========--

// --========================= >> Angular2_Routing | im-mrcoder >> START >> =======--
// import { Component, OnInit } from '@angular/core';
// import { Http, Response } from '@angular/http';
// import { Router} from '@angular/router';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';

// import { IProduct } from './content/products/products';
// import { ProductService } from './content/products/products.service';

// @Component({
//     selector: 'im-mrcoder',
//     templateUrl: 'app/app.component.html',
//     providers: [ProductService]
// })

// export class AppComponent implements OnInit{
//     name: string = 'Mr. Mik Patel';
//     designation: string = 'Software Developer';
//     cellNumber: number = 9974300025;
//     skypeAddress: string = 'MrCoder';
//     mailAddress: string = 'mik@mrcoder.in';
//     website: string = 'www.mrcoder.in';
//     isVisible: boolean = true;

//     errorMessage: string = '';
//     public iProducts: IProduct[] = [];
//     constructor(public _product: ProductService) {
//     }

//     ngOnInit(): void {
//         this._product.getProducts()
//             .subscribe(data => this.iProducts = data, error => this.errorMessage = error);
//     }
// }
// --========================= >> Angular2_Routing | im-mrcoder >> END >> =========--

// --========================= >> Angular2_Navigation | im-mrcoder >> START >> =======--
import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { IProduct } from './content/products/products';
import { ProductService } from './content/products/products.service';

@Component({
    selector: 'im-mrcoder',
    templateUrl: 'app/app.component.html',
    providers: [ProductService]
})

export class AppComponent implements OnInit{
    name: string = 'Mr. Mik Patel';
    designation: string = 'Software Developer';
    cellNumber: number = 9974300025;
    skypeAddress: string = 'MrCoder';
    mailAddress: string = 'mik@mrcoder.in';
    website: string = 'www.mrcoder.in';
    isVisible: boolean = true;

    errorMessage: string = '';
    public iProducts: IProduct[] = [];
    constructor(public _product: ProductService, public _router: Router) {
    }

    ngOnInit(): void {
        this._product.getProducts()
            .subscribe(data => this.iProducts = data, error => this.errorMessage = error);
    }
}
// --========================= >> Angular2_Navigation | im-mrcoder >> END >> =========--
