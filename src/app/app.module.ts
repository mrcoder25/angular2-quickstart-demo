// --========================= >> Initial | my-app >> START >> =======--
// import { NgModule }      from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { AppComponent }  from './app.component';

// @NgModule({
//   imports:      [ BrowserModule ],
//   declarations: [ AppComponent],
//   bootstrap:    [ AppComponent]
// })
// export class AppModule { }
// --========================= >> Initial | my-app >> END >> =========--

// --========================= >> Angular2_MrCoder | im-mrcoder >> START >> =======--
// import { NgModule }      from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';

// import { AppComponent }  from './app.component';
// import { ContentComponent }  from './content/content.component';

// @NgModule({
//   imports:      [ BrowserModule ],
//   declarations: [ AppComponent, ContentComponent ],
//   bootstrap:    [ AppComponent, ContentComponent ]
// })
// export class AppModule { }
// --========================= >> Angular2_MrCoder | im-mrcoder >> END >> =========--

// --========================= >> Angular2_CRUD_Operations_Using_HTTP | im-mrcoder >> START >> =======--
// import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { HttpModule } from '@angular/http';

// import { AppComponent } from './app.component';
// import { ContentComponent } from './content/content.component';

// @NgModule({
//   imports: [BrowserModule, HttpModule],
//   declarations: [AppComponent, ContentComponent],
//   bootstrap: [AppComponent]
// })
// export class AppModule { }
// --========================= >> Angular2_CRUD_Operations_Using_HTTP | im-mrcoder >> END >> =========--

// --========================= >> Angular2_Routing/Angular2_Navigation | im-mrcoder >> START >> =======--
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ContentComponent } from './content/content.component';
import { HideContentComponent } from './content/hide-content.component';
import { PageNotFoundComponent } from './content/page-notfound.component';

const appRoutes: Routes = [
  { path: 'Show', component: ContentComponent },
  { path: 'Hide', component: HideContentComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [BrowserModule, HttpModule, RouterModule.forRoot(appRoutes)],
  declarations: [AppComponent, ContentComponent, HideContentComponent, PageNotFoundComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
// --========================= >> Angular2_Routing/Angular2_Navigation | im-mrcoder >> END >> =========--
