// --========================= >> Angular2_MrCoder | my-content >> START >> =======--
// import { Component } from '@angular/core';

// @Component ({
//    selector: 'my-content',
//    templateUrl: './content.component.html'
// })

// export class ContentComponent {
//    isVisible: boolean = false;
//    dataList: any[] = [ {
//                         "ID": "1",
//                         "Name" : "MrCoder"
//                     },

//                     {
//                         "ID": "2",
//                         "Name" : "Mik"
//                     } ];
// }
// --========================= >> Angular2_MrCoder | my-content >> END >> =========--

// --========================= >> Angular2_DataBinding | my-content >> START >> =======--
// import { Component } from '@angular/core';

// @Component ({
//    selector: 'my-content',
//    templateUrl: './content.component.html'
// })

// export class ContentComponent {
//     isVisible: boolean = true;
//     imageList: any[] = [ {
//                          "ID": "1",
//                          "Url" : "app/app_data/images/MrCoder_Icon.png"
//                      }];
// }
// --========================= >> Angular2_DataBinding | my-content >> END >> =========--

// --========================= >> Angular2_Class_Extends | my-content >> START >> =======--
// import { Component } from '@angular/core';
// import { AppComponent } from '../app.component';

// @Component ({
//    selector: 'my-content',
//    templateUrl: './content.component.html'
// })

// export class ContentComponent extends AppComponent {
//     childIsVisible: boolean = this.isVisible;
//     childImageList: any[] = this.imageList;
// }
// --========================= >> Angular2_Class_Extends | my-content >> END >> =========--

// --========================= >> Angular2_CRUD_Operations_Using_HTTP | my-content >> START >> =======--
// import { Component } from '@angular/core';
// import { AppComponent } from '../app.component';

// @Component({
//     selector: 'my-content',
//     templateUrl: './content.component.html'
// })

// export class ContentComponent extends AppComponent {
//     childIsVisible: boolean = this.isVisible;
//     // childIProduct: IProduct[] = this.iProducts;
// }
// --========================= >> Angular2_CRUD_Operations_Using_HTTP | my-content >> END >> =========--

// --========================= >> Angular2_Routing | my-content >> START >> =======--
// import { Component } from '@angular/core';
// import { AppComponent } from '../app.component';

// @Component({
//     selector: 'my-content',
//     templateUrl: './content.component.html'
// })

// export class ContentComponent extends AppComponent {
//     childIsVisible: boolean = this.isVisible;
// }
// --========================= >> Angular2_Routing | my-content >> END >> =========--

// --========================= >> Angular2_Navigation | my-content >> START >> =======--
import { Component } from '@angular/core';
import { Router }  from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
    selector: 'my-content',
    templateUrl: './content.component.html'
})

export class ContentComponent extends AppComponent {
    childIsVisible: boolean = this.isVisible;

    onBack(): void { 
        this._router.navigate(['/Hide']); 
    } 
}
// --========================= >> Angular2_Navigation | my-content >> END >> =========--