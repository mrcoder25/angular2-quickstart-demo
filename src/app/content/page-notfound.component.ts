// --========================= >> Angular2_Routing/Angular2_Navigation | my-page-not-found >> START >> =======--
import { Component } from '@angular/core';

@Component ({  
   selector: 'my-page-not-found',  
   template: '<span style="color:red">Oops! Page Not Found.</span> Please do click on "Show Content" link to show content.<br/>', 
})  
export class PageNotFoundComponent {  
} 
// --========================= >> Angular2_Routing/Angular2_Navigation | my-page-not-found >> END >> =========--