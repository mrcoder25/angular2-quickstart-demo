// --========================= >> Angular2_Routing | my-hide-content >> START >> =======--
// import { Component } from '@angular/core'
// import { AppComponent } from '../app.component'

// @Component({
//     selector: 'my-hide-content',
//     templateUrl: './content.component.html'
// })

// export class HideContentComponent extends AppComponent {
//     childIsVisible: boolean = !this.isVisible;
// }
// --========================= >> Angular2_Routing | my-hide-content >> END >> =========--

// --========================= >> Angular2_Navigation | my-hide-content >> START >> =======--
import { Component } from '@angular/core'
import { AppComponent } from '../app.component'

@Component({
    selector: 'my-hide-content',
    templateUrl: './content.component.html'
})

export class HideContentComponent extends AppComponent {
    childIsVisible: boolean = !this.isVisible;

    onBack(): void { 
        this._router.navigate(['/Show']); 
    } 
}
// --========================= >> Angular2_Navigation | my-hide-content >> END >> =========--`